from InfovisCorpus import *
from pickle_helpers import *
import itertools
from nltk import trigrams
from pyes import *
import redis
import csv
from datetime import date, timedelta
from progressbar import ProgressBar
pbar = ProgressBar()


parties = {}
party_list = []
countries = ['it' , 'uk' , 'fr' , 'nl']

for country in countries:
	with open('parties_'+country+'.csv', 'rb') as csvfile:
		spamreader = csv.reader(csvfile, delimiter=',')
		for row in spamreader:
			if row[5] and '@' in row[5]:
				key = row[5][1:]
				party_list += [key]
				parties[key]={}
				parties[key]['type']= row[1]
				parties[key]['level']= row[4]
				parties[key]['id']= row[6]
				parties[key]['country']= country


r = redis.StrictRedis(host='localhost', port=6379, db=0)
languages = ['nl' , 'it' , 'en' , 'fr']

for lang in languages:
	r.delete(lang)
	r.delete(lang +"_dict")


# bigram_finder = BigramCollocationFinder.from_words(words)
# bigrams = bigram_finder.nbest(BigramAssocMeasures.chi_sq, n)
n = 200
print "redis..."

domain = "twitter"
language = None
party = None
origin = None
hashtag = None
query = "immigration"
since = None
until = None

ago = 1
interval = 1
today  = date.today().strftime('%Y-%m-%dT%H:%M:%S')
start_date = (date.today() - timedelta(ago)).strftime('%Y-%m-%dT%H:%M:%S')
end_date = (date.today() - timedelta(ago-interval)).strftime('%Y-%m-%dT%H:%M:%S')

print start_date
print end_date

corpus = InfovisCorpus(since=start_date , until=end_date)
print corpus.len()

r.flushall()

for vector in corpus:
	if vector:
		for trigram in trigrams(vector[0]):
			key = ":".join(sorted(trigram))
			r.zincrby(key,'total_frequency', 1)
			r.zincrby(key,vector[1], 1)
			if vector[2] in	party_list:
				r.zincrby(key,vector[1]+"_"+parties[vector[2]]['country'], 1)
				r.zincrby(key,vector[1]+"_"+parties[vector[2]]['country']+"_"+vector[2]+"_o", 1)
				for hashtag in vector[3]:
					r.zincrby(key,vector[1]+"_"+parties[vector[2]]['country']+"_"+vector[2]+"_o_"+hashtag['text'], 1)
			else:
				for mention in vector[4]:
					if mention in party_list:
						r.zincrby(key,vector[1]+"_"+parties[mention]['country'], 1)
						r.zincrby(key,vector[1]+"_"+parties[mention]['country']+"_"+mention+"_p", 1)
						for hashtag in vector[3]:
							r.zincrby(key,vector[1]+"_"+parties[mention]['country']+"_"+mention+"_p_"+hashtag['text'], 1)


conn = ES('141.138.194.193:9200') # Use HTTP

alltrigrams =  r.keys('*')

for trigram in pbar(alltrigrams):

	item =  dict(r.zrange(trigram,0,r.zcard(trigram),withscores=True))
	item['trigram'] = " ".join(trigram.split(':'))
	item['start_date'] = start_date
	item['end_date'] = end_date
	conn.index(item, "trigrams_index", "trigram") 
	