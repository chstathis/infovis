import csv
import redis
from nltk.stem.snowball import SnowballStemmer
import nltk
from pattern.es import parse as parse_es , split
import re
from datetime import date, timedelta , datetime
from unidecode import unidecode
import enchant
from pattern.en import suggest
from progressbar import ProgressBar
# from spellchecker import *


def preprocess(self , document , stoplist):
    return [ re.sub(r'\W+','', token) for token in  document['text'].lower().split() if not (token.startswith('http') or token.startswith('pic.') or token in stoplist)]
  
r = redis.StrictRedis(host='localhost', port=6379, db=0)
r.flushall()
pbar = ProgressBar(maxval=3300)
pbar.start()
stopwords = nltk.corpus.stopwords.words('spanish')  + ["http"]

parties = ['aes' , 'an' , 'dn' , 'edm' , 'falange'  , 'msr' , 'np' , 'pp' , 'pxc']

for party in parties:
  filename = party + ".tab"
  print filename
  # with open(filename, 'rb') as csvfile:
  #     spamreader = csv.reader(csvfile, delimiter='\t')
  #     i = 0
  #     for row in spamreader:
  #       i+=1
  #       pbar.update(i)
  #       if i > 1:
  #         tokenised_text = []
  #         text = row[2]
  #         text = text.decode('utf-8')
  #         text = unidecode(text)
  #         tokenised_text = preprocess(text,stopwords)
  #         date = datetime.strptime(row[3][0:10],'%Y-%m-%d').strftime('%y%m%d')
  #         for x1, term in enumerate(tokenised_text):
  #           start = max(x1-2,0)
  #           end = min(len(tokenised_text),x1+2)+1
  #           for x2,cooc in enumerate(tokenised_text[start:end]):
  #             distance = str(start + x2 - x1)
  #             if distance == '0':
  #               r.zincrby("comments:"+date+":0"  , term , 1) 
  #             else:
  #               r.zincrby("comments:"+date+":" + distance  , term + " " + cooc , 1) 

pbar.finish()
