#!/usr/bin/python
from facepy import GraphAPI
import facepy
from pyes import *
import json
import requests
from datetime import date, timedelta
import datetime



#Authorisation info
application_id = "350264468445684"
application_secret_key = "dbd6129e08dce89d771e91eb404f715e"
short_access_token  = 'CAAEZBkE04GfQBADMmEU3K7dgy7PwwGqZBpHb2TqWE60FXuouSSKEzeWYQ6DJyzJcCEMJya532wo7Rx412ZB7NdjOOi44CNzGRpIWyD8ppx4Jtrpi5DQjeAZCGYE4TYRG1jo6ZCDgfpsZA4ZB4aB2J1TEdWdEjxP16ZBDKlvC0fkfj1Viwa33E9OTDVe7sMiYqxoZD'
extended_access_token = 'CAAEZBkE04GfQBAPJC8p0ZC4oLCgwQcFWBW9ffwR4YQfVNZALNHzOFLwt4wUx3BduGQl8r8NJC6LutdfsTkp66gCE7ZBniA6MZB5ZBTcEJnflBcI28i25YW3ZBDyP6wFJms9Cnp9skuyxEDKXN2RZBlGoyxAzksrO8PzMr9XB5ZCrM0UkfEXquLxpnH2eZBSry2YSQZD'

today = datetime.date.today() 
today= today.strftime("%s")
yesterday = datetime.date.today() - datetime.timedelta(1)
yesterday= yesterday.strftime("%s")

#Get new extended access token!
#extended_access_token, expires_at = facepy.utils.get_extended_access_token(short_access_token, application_id, application_secret_key)
#print "Received extended access until : " , expires_at
#print extended_access_token

conn = ES('141.138.194.193:9200') #Connect to Elasticsearch server!
print "Downloading latest page data from Facebook..."
graph = GraphAPI(extended_access_token)
print "Iterating through pages and downloading yesterdays data... "

likes_data = graph.get('me/likes')['data']
for page in likes_data:
	print "Downloading data for : " , page['name']
	page_data = graph.get(str(page['id'])+'/feed' , options = {'since':str(yesterday) , 'until' : str(today)})
	while page_data:

		# print page_data
		for post in page_data['data']:
			#Some processing here , until we make a proper mapping for ES
			post['created_time'] = datetime.datetime.strptime(post['created_time'],'%Y-%m-%dT%H:%M:%S+0000')
			post['indexed_time'] = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S')

			try:
				c_next = post['comments']['paging']['next']
			except:
				c_next = False
			while c_next:
				comments = json.loads(requests.get(c_next).text)
				try:
					post["comments"]["data"] += comments['data']
					c_next = comments['paging']['next']
				except:
					c_next = False

			try:
				l_next = post['likes']['paging']['next']
			except:
				l_next = False

			while l_next:
				likes = json.loads(requests.get(l_next).text)
				try:
					post['likes']['data'] += likes['data']
					l_next = likes['paging']['next']
				except:
					l_next = False

			#Index Post in ES.
			conn.index(post, "infovis_facebook", "post", post['id'])
			print "Indexing items in ElasticSearch..." 

		# try:
		# 	next_page_data = page_data['paging']['next']
		# 	page_data = json.loads(requests.get(next_page_data).text)
		# except:
		page_data = False



