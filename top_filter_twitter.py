from pyes import *
import json
from pickle_helpers import *
from guess_language import guess_language
from datetime import date, timedelta
from progressbar import ProgressBar
pbar = ProgressBar()

conn = ES('141.138.194.193:9200')


ago = 2
interval = 2
today  = date.today().strftime('%Y-%m-%dT%H:%M:%S')
start_date = (date.today() - timedelta(ago)).strftime('%Y-%m-%dT%H:%M:%S')
end_date = (date.today() - timedelta(ago-interval)).strftime('%Y-%m-%dT%H:%M:%S')

#q = TermQuery("name", "joe") alternative
#q = MatchAllQuery()
q = FilteredQuery(MatchAllQuery(),ANDFilter([RangeFilter(ESRangeOp('created_at', 'gte', start_date)),RangeFilter(ESRangeOp('created_at', 'lt', end_date))]))
results = conn.search(query = q , indices=['infovis_twitter'] )
for item in pbar(results):

	#basic info
	stream_item = {}
	stream_item['id'] = item._meta.id
	stream_item['timestamp'] = item['created_at']
	stream_item['text'] = item['text']
	stream_item['lang']	= guess_language(item['text'])
	stream_item['domain'] = 'twitter'

	#related entities
	stream_item['user'] =  item['user']['screen_name'] 
	stream_item['related_to']  = []
	for mention in item['mention']:
		stream_item['related_to'] += [mention['screen_name']]

	stream_item['hashtag'] = item['hashtag']
 	#This is the retweeded post for tweets and the parent post for facebook.
 	if 'retweet' in item:
 		stream_item['parent_id'] = item['retweet']['id']

	conn.index(stream_item, "infovis_stream", "stream_item", item._meta.id)


