0from InfovisCorpusGensim import *
from pickle_helpers import *

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


# # lsa = gensim.models.lsimodel.LsiModel(corpus=corpus, id2word=corpus.dictionary, num_topics=10)
# # lsi.print_topics(10)
# conn = ES('141.138.194.193:9200')
# q = FilteredQuery( MatchAllQuery(), ANDFilter([RangeFilter(ESRangeOp('created_at', 'lt', '2014-03-12')), RangeFilter(ESRangeOp('created_at', 'gte', '2014-03-11'))]))
# results = conn.search(query = q, indices=['infovis_twitter'])
# tweets = []
# print "starting loop..."
# i = 0
# for result in results:
# 	i+=1
# 	print i
# 	tweets += [result.copy()]
	
# tweet_text = []
# # save_pickle(tweets , "infovis_twitter.pkl")
# tweets = load_pickle("infovis_twitter.pkl")
# i = 0
# for tweet in tweets:
# 	i+=1
# 	print i
# 	tweet_text += [tweet['text']]

# print tweet_text
# save_pickle(tweet_text,"infovis_twitter_text.pkl")

# tweet_text = load_pickle("infovis_twitter_text.pkl")
# # for tweet in tweet_text:
# # 	print tweet

# english_stopwords = load_pickle("english_stopwords.pkl")
# dutch_stopwords = load_pickle("dutch_stopwords.pkl")
# italian_stopwords = load_pickle("italian_stopwords.pkl")
# french_stopwords = load_pickle("french_stopwords.pkl")
# stoplist = set(english_stopwords + dutch_stopwords + italian_stopwords + french_stopwords)

# texts = [[word for word in document.lower().split() if word not in stoplist] for document in tweet_text]
# all_tokens = sum(texts, [])
# tokens_once = set(word for word in set(all_tokens) if all_tokens.count(word) == 1)
# texts = [[word for word in text if word not in tokens_once] for text in texts]

# print texts
# lda = gensim.models.ldamodel.LdaModel(corpus=tweet_text, alpha='auto')
# save_pickle(lda,"lda_model.pkl")

# print "############################################################################################################"
# print "############################################################################################################"
# print "############################################################################################################"
# print lda.print_topics(5)
# print "############################################################################################################"
# print "############################################################################################################"
# print "############################################################################################################"
