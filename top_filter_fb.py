from pyes import *
import json
from pickle_helpers import *
from guess_language import guess_language

conn = ES('141.138.194.193:9200')

#q = TermQuery("name", "joe") alternative
#q = MatchAllQuery()
q = q = MatchAllQuery()
results = conn.search(query = q , indices=['infovis_facebook'])
for item in results:
	stream_item = {}
	stream_item['domain'] = 'facebook'
	stream_item['id'] = item._meta.id
	stream_item['timestamp'] = item['created_time']
	
	if 'message' in item:
		stream_item['text'] = item['message']
		stream_item['lang']	= guess_language(item['message'])
	else:
		stream_item['text'] ="[no_text_post]"

	stream_item['user'] = [ item['from']['name'] ]

	if "comments" in item:
		for comment in item['comments']['data']:
	 		new_stream_item = {}
	 		new_stream_item['domain'] = 'facebook'
			new_stream_item['id'] = comment['id']
			new_stream_item['timestamp'] = comment['created_time']
			new_stream_item['parent_id'] = stream_item['id']
	 		new_stream_item['related_to'] =  stream_item['related_to'] 
			if 'message' in comment:
				new_stream_item['text'] = comment['message']
				stream_item['lang']	= guess_language(comment['message'])
			else:
				new_stream_item['text'] ="[no_text_post]"
	 			

	 		conn.index(new_stream_item, "infovis_stream", "stream_item", new_stream_item['id'] )

	conn.index(stream_item, "infovis_stream", "stream_item", item._meta.id)


