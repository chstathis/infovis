from InfovisCorpus import *
from pickle_helpers import *
import itertools
from nltk import bigrams
from pyes import *
import redis
import csv
from datetime import date, timedelta , datetime
from progressbar import ProgressBar
pbar = ProgressBar()


parties = {}
party_list = []
countries = ['it' , 'uk' , 'fr' , 'nl']

fb_accounts = {}
for country in countries:
	with open('parties_'+country+'.csv', 'rb') as csvfile:
		spamreader = csv.reader(csvfile, delimiter='\t')
		for row in spamreader:
			if row[5] and '@' in row[5]:
				key = row[5][1:].lower()
				party_list += [key]
				parties[key]={}
				parties[key]['type']= row[1]
				parties[key]['level']= row[4]
				parties[key]['id']= row[6]
				parties[key]['country']= country
				if row[8]:
				  	fb_accounts[row[8]] = key

# print party_list
# print fb_accounts
r = redis.StrictRedis(host='localhost', port=6379, db=0)
languages = ['nl' , 'it' , 'en' , 'fr']

for lang in languages:
	r.delete(lang)
	r.delete(lang +"_dict")


# bigram_finder = BigramCollocationFinder.from_words(words)
# bigrams = bigram_finder.nbest(BigramAssocMeasures.chi_sq, n)
n = 200
print "redis..."

domain = "twitter"
language = None
party = None
origin = None
hashtag = None
query = "immigration"
since = None
until = None

ago = 3
interval = 1
today  = date.today().strftime('%Y-%m-%dT%H:%M:%S')
# start_date = (date.today() - timedelta(ago)).strftime('%Y-%m-%dT%H:%M:%S')
# end_date = (date.today() - timedelta(ago-interval)).strftime('%Y-%m-%dT%H:%M:%S')
# last_run = load_pickle("last_run.pkl")
# save_pickle(today,"last_run.pkl")
corpus = InfovisCorpus()
print corpus.len()
r.flushall()
i = 0
for doc in corpus:
	print i
	i+=1
	key = 0
	domain = 0
	party = 0
	source = 0
	date = 0

	if 	type(doc["date"]) is str:
		check_date = datetime.strptime(doc["date"][0:10],'%Y-%m-%d')
		week = check_date.isocalendar()[1]
		date = check_date.strftime('%y') + ":" + str(week)
	else:
		check_date = doc["date"]
		week = check_date.isocalendar()[1]
		date = check_date.strftime('%y') + ":" + str(week)

	if True:
		if doc["domain"] =="twitter":
			domain = 't'
			if doc["user"] in party_list:
				party = doc["user"]
				source = "o"
			else:
				for mention in doc["mentions"]:
					ment = mention["screen_name"].lower()
					if ment in party_list:
		 				party = ment
		 				source = "p"
		 		if "retweet" in doc:
			 		ment = doc["retweet"]["user_screen_name"].lower()
					if ment in party_list:
		 				party = ment
		 				source = "p"
		elif doc["domain"] =="facebook":
			domain = 'f'
			if doc["user"] in fb_accounts and fb_accounts[doc["user"]] in party_list:
				party = fb_accounts[doc["user"]]
				if doc["comment"]:
					source = "p"
				else:
					source = "o"
					
		

		if date and domain and party and source :
			country = parties[party]['country']
			for x1, term in enumerate(doc['text']):
				if term != party:
					start = max(x1-2,0)
					end = min(len(doc['text']),x1+2)+1
					for x2,cooc in enumerate(doc['text'][start:end]):
						if cooc != party:
							distance = str(start + x2 - x1)
							if distance == '0':
								r.zincrby("all:"+ date +":0"  , term , 1)
								r.zincrby(country+ ":" + date +":0"  , term , 1)
								r.zincrby(party+":all:" + date +":0"  , term , 1) 
								r.zincrby(party+":"+domain+":"+source+":"+date+":0"  , term , 1) 

							else:
								r.zincrby("all:"+ date+":" + distance , term + " " + cooc , 1)
								r.zincrby(country +":"+ date+":" + distance  , term + " " + cooc , 1)
								r.zincrby(party+":all:" + date +":0"   ,  term + " " + cooc  , 1) 
								r.zincrby(party+":"+domain+":"+source+":"+date+":" + distance  , term + " " + cooc , 1) 
					
		

	# print doc['text']
	# for bigram in list(set(sorted(itertools.permutations(doc['text'],2)))):
	# 	print bigram
		# distance =str(abs(vector[0].index(bigram[0]) - vector[0].index(bigram[1])));
		# if vector[2] in	party_list:
		# 	for hashtag in vector[3]:
		# 		r.zincrby(date+":"+vector[1]+":"+parties[vector[2]]['country']+":"+vector[2]+":o:"+hashtag['text']+":"+distance , " ".join(sorted(bigram)) , 1) 
		# else:
		# 	for mention in vector[4]:
		# 		if mention in party_list:
		# 			for hashtag in vector[3]:
		# 				r.zincrby(date +":"+vector[1]+":"+parties[mention]['country']+":"+mention+":p:"+hashtag['text']+":"+distance," ".join(sorted(bigram)) , 1)

			

# conn = ES('141.138.194.193:9200') # Use HTTP

# alltrigrams =  r.keys('*')

# for trigram in pbar(alltrigrams):

	# item =  dict(r.zrange(trigram,0,r.zcard(trigram),withscores=True))
	# item['trigram'] = " ".join(trigram.split(':'))
	# item['start_date'] = start_date
	# item['end_date'] = end_date
	# conn.index(item, "trigrams_index", "trigram") 
	# 