import logging, gensim, bz2
from gensim import corpora, models, similarities
from pyes import *
from pyes.queryset import generate_model
import nltk
import re
import enchant
from nltk.stem.snowball import SnowballStemmer
from time import strftime
from guess_language import guess_language
from pattern.it import parse as parse_it
from pattern.en import parse as parse_en
from pattern.nl import parse as parse_nl
from pattern.fr import parse as parse_fr
from load_party_info import *
from pickle_helpers import *


class InfovisCorpusGensim(object):
	def __init__(self):
		self.conn = ES('141.138.194.193:9200')
		self.q = MatchAllQuery()		
		self.languages = {
			'en' : ['english',  lambda x: parse_en(x.encode('utf-8'), tokenize=True, tags=False, chunks=False, relations=False, lemmata=True, encoding="utf-8", tagset=None)],
			'fr' : ['french',  lambda x: parse_fr(x.encode('utf-8'), tokenize=True, tags=False, chunks=False, relations=False, lemmata=True, encoding="utf-8", tagset=None)], 
			'nl' : ['dutch',  lambda x: parse_nl(x.encode('utf-8'), tokenize=True, tags=False, chunks=False, relations=False, lemmata=True, encoding="utf-8", tagset=None)],
			'it' : ['italian', lambda x: parse_it(x.encode('utf-8'), tokenize=True, tags=False, chunks=False, relations=False, lemmata=True, encoding="utf-8", tagset=None)]
		}
		self.domain_specific_stoplist = ['rt' , 'http' , 'www'] 
		
		self.results = self.conn.search(query = Search(self.q), indices=['infovis_twitter'])
		self.length = self.results.count()
		self.collect_statistics()
		self.dictionary = load_pickle("lda_dict.pkl")

	
	def collect_statistics(self):
		self.dictionary = corpora.Dictionary(self.preprocess(result) for result in self.results)
		stoplist = nltk.corpus.stopwords.words("english") +  nltk.corpus.stopwords.words("french") + nltk.corpus.stopwords.words("dutch") +  nltk.corpus.stopwords.words("italian") + self.domain_specific_stoplist
		english_stopwords = load_pickle("english_stopwords.pkl")
		dutch_stopwords = load_pickle("dutch_stopwords.pkl")
		italian_stopwords = load_pickle("italian_stopwords.pkl")
		french_stopwords = load_pickle("french_stopwords.pkl")
		stoplist = set(stoplist + english_stopwords + dutch_stopwords + italian_stopwords + french_stopwords)
		stop_ids = [self.dictionary.token2id[stopword] for stopword in stoplist if stopword in self.dictionary.token2id ]
		once_ids = [tokenid for tokenid, docfreq in self.dictionary.dfs.iteritems() if docfreq == 1]
		self.dictionary.filter_tokens(stop_ids + once_ids)
		self.dictionary.compactify()
		
	def preprocess(self , document):
		return [ re.sub(r'\W+','', token) for token in  document['text'].lower().split() if not (token.startswith('http') or token.startswith('pic.'))]
		

	def __iter__(self):
		for result in self.results:
			 yield self.dictionary.doc2bow(self.preprocess(result))

		

