import logging, gensim, bz2
from gensim import corpora, models, similarities
from pyes import *
from pyes.queryset import generate_model
import nltk
import re
import enchant
from nltk.stem.snowball import SnowballStemmer
from time import strftime
from guess_language import guess_language
from pattern.it import parse as parse_it
from pattern.en import parse as parse_en
from pattern.nl import parse as parse_nl
from pattern.fr import parse as parse_fr
from pickle_helpers import *
from unidecode import unidecode
from nltk.stem.porter import PorterStemmer

class InfovisCorpus(object):
	def __init__(self,since=None,until=None):
		self.conn = ES('141.138.194.193:9200')

		if not since :
			self.since = "0"
		else:
			self.since = since

		if not until:
			self.until = strftime("%Y-%m-%dT%H:%M:%S")
		else:
			self.until = until
		self.q = MatchAllQuery()
		# self.q = FilteredQuery( MatchAllQuery(), ANDFilter([RangeFilter(ESRangeOp('created_at', 'lt', '2014-03-12')), RangeFilter(ESRangeOp('created_at', 'gte', '2014-03-11'))]))
		self.languages = {
			'en' : ['english',  lambda x: parse_en(x.encode('utf-8'), tokenize=True, tags=False, chunks=False, relations=False, lemmata=True, encoding="utf-8", tagset=None)],
			'fr' : ['french',  lambda x: parse_fr(x.encode('utf-8'), tokenize=True, tags=False, chunks=False, relations=False, lemmata=True, encoding="utf-8", tagset=None)], 
			'nl' : ['dutch',  lambda x: parse_nl(x.encode('utf-8'), tokenize=True, tags=False, chunks=False, relations=False, lemmata=True, encoding="utf-8", tagset=None)],
			'it' : ['italian', lambda x: parse_it(x.encode('utf-8'), tokenize=True, tags=False, chunks=False, relations=False, lemmata=True, encoding="utf-8", tagset=None)]
		}
		self.domain_specific_stoplist = ['rt' , 'http' , 'www'] #add more here
		self.length = 0;
		self.collect_statistics()
		self.tags_to_keep = []
		self.stoplist = nltk.corpus.stopwords.words("english") +  nltk.corpus.stopwords.words("french") + nltk.corpus.stopwords.words("dutch") +  nltk.corpus.stopwords.words("italian") + self.domain_specific_stoplist
		self.stemmer = PorterStemmer()
		# self.stoplist = []
		# for stopword in self.oldstoplist:
		# 	self.stoplist += [unidecode(stopword.encode('utf-8' , 'ignore'))]
		
	def len(self):
		return self.length

	def collect_statistics(self):
		results = self.conn.search(query = self.q, indices=['infovis_twitter'])
		self.length = results.count()
		results = self.conn.search(query = self.q, indices=['infovis_facebook'])
		self.length += results.count()
		
	def preprocess(self , document):
		# return [token for token in nltk.word_tokenize(re.sub(r'\W+',' ',unidecode(document.lower()).encode('utf-8'))) if not ( self.stemmer.stem(token) in self.stoplist)]
		#text = document.lower().split(" ")
		# tokenised_text = []
		# for term in text:
		# 	if not ( term.startswith("http") or term.startswith("pic")):
		# 		terms = re.sub(r'\W+',' ',unidecode(term).encode('utf-8')).split(" ")
		# 		for sterm in terms:
		# 			if not (sterm in self.stoplist or len(sterm)<4 ):
		# 				tokenised_text += [sterm]
		# return tokenised_text

		# text = re.sub(r'\W+',' ',document.lower()).encode('utf-8')
		# tokenised_text =[word for word in nltk.word_tokenize(text) if word not in self.stoplist and len(word)>3 ]
		# return tokenised_text
		text = unidecode(document).encode('utf-8')
		language = guess_language(document)
		if language in self.languages:
			lemmas = self.languages[language][1](text)
			tokenised_text = []
			for w in lemmas.split(" "):
				lemma = w.split("/")
				if  len(lemma[2]) > 2 and  ( lemma[2] not in self.stoplist and lemma[1][0] in  ['J' , 'N' , 'V' , 'R'] ) :
					tokenised_text += [ lemma[2] ]
			return tokenised_text
		else:
			return []

	def __iter__(self):
		results = self.conn.search(query = self.q, indices=['infovis_facebook'] , scan=True)
		for result in results:
			doc = {}
			doc['domain'] = 'facebook'
			doc['date'] = result['created_time']
			doc['user'] = result['from']['name']
			doc['comment'] = False
			if 'message' in result:
				doc['text'] = self.preprocess(result['message'])
			else:
				doc['text'] = ""
			if "comments" in result:
				for comment in result['comments']['data']:
			 		new_doc = {}
			 		new_doc['domain'] = 'facebook'
					new_doc['date'] = comment['created_time']
					new_doc['user'] = result['from']['name']
					new_doc['comment'] = True
					if 'message' in comment:
						new_doc['text'] = self.preprocess(comment['message'])
					else:
						new_doc['text'] =""

					yield new_doc
			yield doc

		results = self.conn.search(query = self.q, indices=['infovis_twitter'] , scan=True)
		for result in results:
			doc = { "text" : self.preprocess(result["text"]) ,"user" : result['user']["screen_name"].lower() , "mentions" :result['mention'] , "domain" : "twitter" , "date" : result['created_at']}
			if "retweet" in result:
				doc["retweet"] = result["retweet"]
			yield doc

		


			
	def porter(tokenised_text):
		st = PorterStemmer()
		stemmed_tokens = []
		for token in tokenised_text:
			stemmed_tokens += [st.stem(token)]

		return stemmed_tokens


