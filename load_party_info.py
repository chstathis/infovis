import csv

def load_party_info():
	parties = {}
	party_list = []
	countries = ['it' , 'uk' , 'fr' , 'nl']



	fb_accounts = {}
	for country in countries:
		with open('parties_'+country+'.csv', 'rb') as csvfile:
			spamreader = csv.reader(csvfile, delimiter='\t')
			for row in spamreader:
				if row[5] and '@' in row[5]:
					key = row[5][1:].lower()
					party_list += [key]
					party={}
					party['account'] = key
					party['type']= row[1]
					party['level']= row[4]
					party['id']= row[6]
					party['country']= country
					party['name']= row[2]

					if country in parties:
						if party['name'] in parties[country]:
							parties[country][party['name']]  += [party]
						else:
							parties[country][party['name']] = [party]
					else:
						parties[country] = {}
						parties[country][party['name']] = [party]

	return parties , party_list 