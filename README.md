Intro:
=======
We are currently gathering live Twitter data using an ElasticSearch Plugin that can follow accounts and keywords (we only have accounts now) and Facebook data using a script that runs every 2 hours and gets the latest posts from every page that "Guapo" - our fake facebook profile  - likes.

REDIS:
------
I think the best way to keep track of counts for this project where we need to group by  language , domain , party and date is using Redis with a sorted set for every combination , and then using the unio functions to get aggregated results.

TODO:
-----

    -Start Implementig LDA topic tracking system.
    -Add Facebook pages of extremist parties.

ACCOUNTS INFORMATION:
=====================
Mysql info:
-----------

    user : infovis
    password : okd@5Ekn#A83M2e


Twitter User Info:
------------------

    OAUTH_KEYS = {'consumer_key': 'HsPGaQC0XXEsWUiW4CTA',
                  'consumer_secret': 'EYNMVZbrezmVoMP73TrrnCwJxf7QDo2j7gf43lx161Y',
                  'access_token_key': '2276473639-imXt1OMu723ldEaj50kC9s5jHyjP69suDPtCbdG',
                  'access_token_secret': '5ZOGryNl2OfkYRqCDhmF4hRYojeZXmsDfMwJpJbCytpcr'}

Fake Facebook account (and Gmail):
----------------------------------

        user: guapo.fakename@gmail.com
        password: &1u>0_]9UC*)16L


LINKS:
======

Recognising the Language:
-------------------------
I found this small lib for guessing the language of a small text string. Hopefully it wont add much noise.

    https://bitbucket.org/spirit/guess_language

Ok i tried this and its not completely accurate so o also tried the google API but it keeps stoping after a set number of requests, even if a sleep(1) between them , so idk. Another solution is to use a big list of stopwords for text that we dont really know the language. Also  we need to gather some domain specific keywords like 'rt' for twitter.

Enchant:
--------
This is a python library for spellchecking we could use it to filter the tokens so that we dont get garbage
List of dictionaries here:
    http://packages.ubuntu.com/precise/myspell-dictionary


DMI-TCAT:
---------
This is a tool for following twitter users and keywords , it worked fine for keywords but i could make it follow accounts so i moved to the ElasticSearch Solution.

    http://www.iliohari.gr/dmi-tcat/analysis/
    git clone https://stathischaritos:6yujj8n6@github.com/digitalmethodsinitiative/dmi-tcat.git

Gensim:
-------
Python library for using LDA and LSA for topic tracking.

    sudo pip install --upgrade gensim
    http://radimrehurek.com/gensim/intro.html


Topic Tracking algs:
--------------------

LDA:

	http://shuyo.wordpress.com/2011/05/18/latent-dirichlet-allocation-in-python/

LSA:

	http://blog.josephwilk.net/projects/latent-semantic-analysis-in-python.html


TWITTER-ELASTICSEARCH RIVER:
============================
This is and ElasticSearch plugin that uses the river capabilities of ES to index the twitter stream. You can add all settings info you would add for regular ES indexing , and also specify usersand keywords or other tags (geopoint etc.) to track.More info at the link.
link
----
    https://github.com/elasticsearch/elasticsearch-river-twitter


To start the river:
-------------------
Run this script on the command line , if you are at a remote location change localhost to the ES server IP.Now it only has the follow option but we can add the "track" option for keywords.

    curl -XPUT localhost:9200/_river/infovis_twitter/_meta -d '
    {
        "type" : "twitter",
        "twitter" : {
            "oauth" : {
                "consumer_key" : "HsPGaQC0XXEsWUiW4CTA",
                "consumer_secret" : "EYNMVZbrezmVoMP73TrrnCwJxf7QDo2j7gf43lx161Y",
                "access_token" : "2276473639-imXt1OMu723ldEaj50kC9s5jHyjP69suDPtCbdG",
                "access_token_secret" : "5ZOGryNl2OfkYRqCDhmF4hRYojeZXmsDfMwJpJbCytpcr"
            },
            "filter" : {
                "follow" : "13514762,567285403,369479166,245383621,245383621,345257174,781062115,179202323,216200107,465842603,53160005,20509689,1227448789,217749896,41778159,22473403,95972673,117052823,61524357,247721810,704846269,67693131,279157541,358204197,196980761,13514762,567285403,369479166,236362651,334589689,980778746,270839361,1870936627,274574044,972343674,1166143550,495277374,425930225,106673706,436162711,633908233,101573147,254059856,562378202,1487836310,407606658,631275319,583977251,333622261,1075084718,581913885,69532732,289400495,19067940,8873182,237925802,1343975432,938790602,1966444098,1869139004,857642184,1632365562,601929807,821945424,612076857,262702883,1027404487,1024625640,603178613,632797079,419320560,215699843,990052896,216292807,216784891,216650259,562839732,1075102646,615041030,1493885648,602903174,198823867,1299666013,993096637,619839545,1558229390,900076614,954758785,816959106,521357452,980097320,614409542,1924482961"
            }
        }
    }'


Accounts Currently Following:
-----------------------------
These are the IDs for all the twitter accounts we are currently following. We need to add Facebook accounts too.

    2201623465: causes a problem


    13514762,567285403,369479166,245383621,245383621,345257174,781062115,179202323,216200107,465842603,53160005,20509689,1227448789,217749896,41778159,22473403,95972673,117052823,61524357,247721810,704846269,67693131,279157541,358204197,196980761,13514762,567285403,369479166,236362651,334589689,980778746,270839361,1870936627,274574044,972343674,1166143550,495277374,425930225,106673706,436162711,633908233,101573147,254059856,562378202,1487836310,407606658,631275319,583977251,333622261,1075084718,581913885,69532732,289400495,19067940,8873182,237925802,1343975432,938790602,1966444098,1869139004,857642184,1632365562,601929807,821945424,612076857,262702883,1027404487,1024625640,603178613,632797079,419320560,215699843,990052896,216292807,216784891,216650259,562839732,1075102646,615041030,1493885648,602903174,198823867,1299666013,993096637,619839545,1558229390,900076614,954758785,816959106,521357452,980097320,614409542,1924482961


WHAT MAKES A GOOD DATASET:
==========================
This is a small description of our dataset in relation to the characteristics defined by Stephen Few. 

Volume:
-------
With the current settings we are gathering around 5k-6k tweets a day , which would make for a weekly yield of ~50k tweets , adding facebook to the picture will surely double this number. This is live data which means that the more the system runs the higher the volume. To increase the volume even more , for either domain , we would have to increase the size of the entity pool we are currently following. An other option would be to also track important keywords , which however would only work for twitter. Finally , adding historical data in addition to live data would also increase our datasets volume , which however is only an option for Facebook.

Historical:
-----------
Gathering live data means we only have a history since we started the system .Adding historical data is feasible for facebook but not for twitter - not without paying - and since we cant have both domains a decision was made to not get historical data from either.

Consistent:
-----------
Since we have confined our gathering to a specific pool of right wing and populist entities we do not directly keep track of the context (maybe start now guys ??? ). To do that we could track keywords that are relevant to the political context , either defined by us  , or topics that emerge from the current data(both methods would add some bias). Another option would be to also keep track of other political entities from the whole spectrum. If not , we are leaving the understanding of the context to the journalist (or any othe kind of analyst) would will use our tool.

Clean:
------
The information we get is accurate and free of error and complete. We have an initial data gathering step where we get all possible data for either tweets or facebook posts. That includes basic info , but also every possible detail available , like geolocation etc. Hopefully the fact that our sources are legitimate and widely used ensures the accuracy of the data and the lack of error. Cleaning the data only comes down to choosing what fields we are going to use.

Known Pedigree:
---------------
As mentioned above we are gathering data from two very widely used sources. In relation tou our research question , the only thing to make sure is that our data is "honest", meaning that it originated from an entity which actually belongs to the extreme right. The is noise in this process added from "trolls" or from the fact that facebook might block such extreme pages.

Multivariate:
-------------
The initial data we get has various fields (10-20) depending also on the source. Filtering the data , at least for topic tracking , we need to keep only text , timestamp , source domain , and the related entities. But we have a vast pool of variables to choose for extra analysis , like comments , likes , retweets , geolocation etc.

Segmentation:
-------------
Our data can mainly be segmented wrt Language and domain (twitter or facebook).

Clear:
------
THe availlable variables , at least in the final filtered data ,are straightforward.

Atomic:
-------
The data contains alot of aggregated data fields (ex. sum of likes and comments in Facebook). But we still need to use statistical algorithms to retrieve the actual data we will use for visualisation.





Technical Details:
------------------

Twitter:
-------
To gather twitter data we use an elasticsearch plugin found here:
    https://github.com/elasticsearch/elasticsearch-river-twitter

We provide this with accounts and get back a stream of tweets related to those accounts. To use we also need to provide credentials from a twitter app that we created.

Facebook:
---------
To gather facebook data we use a python script that accesses the Facebook Open Graph API. We use credentials from a facebook app we created and through that we access the account of a fake user who follows all the facebook pages we are intrested in. Right now we get the last 25 posts of the pages feed and update them every 6 hours - this is an arbitrary choice, we have to consider each pages post frequency. Using the Open Graph API we could also slowly traverse through the pages history.

Data Storage:
--------------
We store this data in two ElasticSearch Indexes. The setup was done on a VPS server in order for it to run 24/7 and for the data to be availlable to the whole group at any time.

Data Filtering:
---------------
There is a top filtering process that gets the latest data from the two initial indexes and keeps only text and other variables we are interested in , and then indexes this data into a final index that contains all posts from both domains. This index is to be used at least for doing the online LDA algorithm


LDA Process:
------------







ES MAPPING:
===========
I added a mapping cause some fields had prolems ex. couldnt search for "it" in the language field.

curl -XPOST localhost:9200/infovis_stream -d '{
    "settings" : {
        "number_of_shards" : 5        
    },
    "mappings" : {
        "stream_item" : {
            "_source" : { "enabled" : true },
            "properties" : {
                "lang" : { "type" : "string", "index" : "not_analyzed" },
                "timestamp" : { "type" : "date" },
                "text" : {"type" : "string", "index" : "not_analyzed"},
                "domain" : { "type" : "string"},
                "id" :{ "type" : "string", "index" : "not_analyzed" }
            }
        }
    }
}'


http://codeslashslashcomment.com/2012/09/01/search-query-suggestions-using-elasticsearch-via-shingle-filter-and-facets/




13514762,567285403,236362651,270839361,1870936627,495277374,106673706,436162711,1324893229,69532732,938790602,1966444098,1869139004,821945424,612076857,262702883,1027404487,603178613,215699843,990052896,216292807,216784891,216650259,562839732,1075102646,615041030,1493885648,602903174,993096637,816959106,521357452,980097320,614409542,1924482961,78085410,217749896,59591873,491776264,528890147,147988448,1192786104,395351827,347851279,209485030,472412809,162023981,163951423,474963532,71723706,41778159,105161244,212717686,112273454,97027086,381453862,93826417,325066904,279157541,94336415,87939274,358204197,19017675,158021529,221795831,33318995,410224577,374712154,844126038,158550139,256943061,2324261167,1420747592,108955199

2324261167,2199795114,2209485030

14281853

curl -XPUT localhost:9200/_river/infovis_twitter/_meta -d '
{
    "type" : "twitter",
    "twitter" : {
        "oauth" : {
            "consumer_key" : "HsPGaQC0XXEsWUiW4CTA",
            "consumer_secret" : "EYNMVZbrezmVoMP73TrrnCwJxf7QDo2j7gf43lx161Y",
            "access_token" : "2276473639-imXt1OMu723ldEaj50kC9s5jHyjP69suDPtCbdG",
            "access_token_secret" : "5ZOGryNl2OfkYRqCDhmF4hRYojeZXmsDfMwJpJbCytpcr"
        },
        "filter" : {
            "follow" : "13514762,567285403,236362651,270839361,1870936627,495277374,106673706,436162711,1324893229,69532732,938790602,1966444098,1869139004,821945424,612076857,262702883,1027404487,603178613,215699843,990052896,216292807,216784891,216650259,562839732,1075102646,615041030,1493885648,602903174,993096637,816959106,521357452,980097320,614409542,1924482961,13294452,147543162,28528873,289400495,19067940,279157541,94336415,87939274,358204197,19017675,158021529,221795831,33318995,410224577,374712154,844126038,158550139,256943061,1420747592,108955199,14291684,14281853,5680622,78085410,217749896,59591873,491776264,528890147,147988448,1192786104,395351827,347851279,472412809,162023981,163951423,474963532,71723706,41778159,105161244,212717686,112273454,97027086,381453862,93826417,325066904,7343682,7599192,28076891,17289752,95455794,19900973,2201623465,2324261167,2199795114,2209485030",
            "tracks" : "#EU, #EP2014, #Stopimmigration, #exitEU, #Europe, #UE, #EU2014, #troika, #stopislam,#Europeas2014, #LosEspañolesPrimero, #pxc, #España2000, #primerelsdecasa, #stopinmigracion, #inmigracion, #eleccionesUE , Europeas2014, LosEspañolesPrimero, pxc, España2000, primerelsdecasa, stopinmigracion, inmigracion, eleccionesUE"
        }
    }
}'



curl -XPUT localhost:9200/_river/infovis_twitter/_meta -d '
{
    "type" : "twitter",
    "twitter" : {
        "oauth" : {
            "consumer_key" : "...",
            "consumer_secret" : "...",
            "access_token" : "...",
            "access_token_secret" : "..."
        },
        "filter" : {
            "follow" : "2324261167"
        }
    }
}'




Pipeline:

  -get raw tweets(and fb posts)
  -filter and put in initial index
  -get timeinterval and save trigram counts in redis
  -index trigram counts with timeinterval stamp in ES



"ngram_text":{
           "search_analyzer":"analyzer_shingle",
           "index_analyzer":"analyzer_shingle",
           "type":"string"
        }

curl -XPOST localhost:9200/infovis_stream -d '{
    "settings" : {
        "number_of_shards" : 5        
    },
    "mappings" : {
        "stream_item" : {
            "_source" : { "enabled" : true },
            "properties" : {
                "lang" : { "type" : "string", "index" : "not_analyzed" },
                "timestamp" : { "type" : "date" },
                "text" : {"type" : "string", "index" : "not_analyzed"},
                "domain" : { "type" : "string"},
                "id" :{ "type" : "string", "index" : "not_analyzed" }
            }
        }
    }
}'






{
    "filtered" : {
        "query" : {
            "term" : { "name.first" : "shay" }
        },
        "filter" : {
            "or" : [
                {
                    "term" : { "name.second" : "banon" }
                },
                {
                    "term" : { "name.nick" : "kimchy" }
                }
            ]
        }
    }
}




Last update was 3:30 at the 3rd of march










13514762,567285403,236362651,270839361,1870936627,495277374,106673706,436162711,1324893229,69532732,938790602,1966444098,1869139004,821945424,612076857,262702883,1027404487,603178613,215699843,990052896,216292807,216784891,216650259,562839732,1075102646,615041030,1493885648,602903174,993096637,816959106,521357452,980097320,614409542,1924482961,13294452,147543162,28528873,289400495,19067940,279157541,94336415,87939274,358204197,19017675,158021529,221795831,33318995,410224577,374712154,844126038,158550139,256943061,1420747592,108955199,14291684,14281853,5680622,78085410,217749896,59591873,491776264,528890147,147988448,1192786104,395351827,347851279,472412809,162023981,163951423,474963532,71723706,41778159,105161244,212717686,112273454,97027086,381453862,93826417,325066904,7343682,7599192,28076891,17289752,95455794,19900973,2201623465,2324261167,2199795114,2209485030
